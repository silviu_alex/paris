﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pairs_v2.MVVM.Model;
using Pairs_v2.MVVM.View_Model;

namespace Pairs_v2.Operations
{
    public class LogInOperations
    {
        private int index1 = 0;
        private LogInModel logInModel;
        public int Index1 { get => index1; set => index1 = value; }
        public LogInOperations(LogInModel logInModel)
        {
            this.logInModel = logInModel;
        }
        public void OpenAddUserWindow(object param)
        {
            AddUserxaml addusergzaml = new AddUserxaml();
            addusergzaml.Show();
        }
        public void QuitApplication(object param)
        {
            System.Windows.Application.Current.Shutdown();
        }



        public void RightArrowButton(object param)
        {
            if (index1 < 9)
                index1++;
        }
        public void LeftArrowButton(object param)
        {
            if (index1 > 0)
                index1--;
        }
        public string DisplayedImagePath
        {
            get
            {
                switch(index1)
                {
                    case 0:
                        return @"C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar0.png";
                    case 1:
                        return @"C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar1.png";
                    case 2:
                        return @"C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar2.png";
                    case 3:
                        return @"C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar3.png";
                    case 4:
                        return @"C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar4.png";
                    case 5:
                        return @"C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar5.png";
                    case 6:
                        return @"C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar6.png";
                    case 7:
                        return @"C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar7.png";
                    case 8:
                        return @"C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar8.png";
                    case 9:
                        return @"C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar9.png";
                }
                return "C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar0.png";
               
            }
        }
        
    }
}
