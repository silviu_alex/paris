﻿using Pairs_v2.MVVM.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pairs_v2.MVVM.View_Model
{
    public class AvatarVM : BaseClassNotifyPropertyChanged
    {
        public ObservableCollection<AvatarM> Avatars
        {
            get
            {
                return Avatars;
            }
            set
            {
                Avatars = value;
            }
        }
        public AvatarVM()
        {
            Avatars = new ObservableCollection<AvatarM>
            {
                new AvatarM{ Width = 96, Height = 96, AvatarFileName = "avatar0",
                    Path = "C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar0.png"},
                new AvatarM{ Width = 96, Height = 96, AvatarFileName = "avatar1",
                    Path = "C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar1.png"},
                new AvatarM{ Width = 96, Height = 96, AvatarFileName = "avatar2",
                    Path = "C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar2.png"},
                new AvatarM{ Width = 96, Height = 96, AvatarFileName = "avatar3",
                    Path = "C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar3.png"},
                new AvatarM{ Width = 96, Height = 96, AvatarFileName = "avatar4",
                    Path = "C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar4.png"},
                new AvatarM{ Width = 96, Height = 96, AvatarFileName = "avatar5",
                    Path = "C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar5.png"},
                new AvatarM{ Width = 96, Height = 96, AvatarFileName = "avatar6",
                    Path = "C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar6.png"},
                new AvatarM{ Width = 96, Height = 96, AvatarFileName = "avatar7",
                    Path = "C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar7.png"},
                new AvatarM{ Width = 96, Height = 96, AvatarFileName = "avatar8",
                    Path = "C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar8.png"},
                new AvatarM{ Width = 96, Height = 96, AvatarFileName = "avatar9",
                    Path = "C:\\Users\\Silviu-PC\\source\\repos\\Pairs v2\\Pairs v2\\Avatars\\avatar9.png"},
            };

        }
    }
}
