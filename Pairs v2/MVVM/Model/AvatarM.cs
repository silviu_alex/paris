﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pairs_v2.MVVM.Model
{
    public class AvatarM : BaseClassNotifyPropertyChanged
    {
        private int width;
        private int height;
        private string avatarFileName;
        private string path;
        public AvatarM()
        {
        }
        public int Width
        {
            get
            {
                return width;
            }
            set
            {
                width = 96;
                OnPropertyChanged("Width");
            }
        }
        public int Height
        {
            get
            {
                return height;
            }
            set
            {
                height = 96;
                OnPropertyChanged("Height");
            }
        }
        public string AvatarFileName
        {
            set
            {
                avatarFileName = value;
                OnPropertyChanged("AvatarFileName");
            }
            get
            {
                return avatarFileName;
            }
        }
        public string Path
        {
            get
            {
                return path;
            }
            set
            {
                path = value;
                OnPropertyChanged("Path");
            }
        }
    }
}
