﻿using Pairs_v2.MVVM.View_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pairs_v2.MVVM.Model
{
    class GameInitalizing
    {
        public static List<User> Users { get; set; }
        public static List<List<Card>> StandardGameBoardSetUp()
        {
            List<List<Card>> StandardGame = new List<List<Card>>()
            {
                new List<Card>()
                {
                    new Card(0, 0, "Visible", "jph"),
                    new Card(0, 1, "Visible", "jph"),
                    new Card(0, 2, "Visible", "jph"),
                    new Card(0, 3, "Visible", "jph"),
                },
                new List<Card>()
                {
                    new Card(1, 0, "Visible", "jph"),
                    new Card(1, 1, "Visible", "jph"),
                    new Card(1, 2, "Visible", "jph"),
                    new Card(1, 3, "Visible", "jph"),
                },
                new List<Card>()
                {
                    new Card(2, 0, "Visible", "jph"),
                    new Card(2, 1, "Visible", "jph"),
                    new Card(2, 2, "Visible", "jph"),
                    new Card(2, 3, "Visible", "jph"),
                },
                new List<Card>()
                {
                    new Card(3, 0, "Visible", "jph"),
                    new Card(3, 1, "Visible", "jph"),
                    new Card(3, 2, "Visible", "jph"),
                    new Card(3, 3, "Visible", "jph"),
                },
            };
            //for (int i = 0; i < 5; i++)
            //    for(int j = 0; j < 5; j++)
            //    {
            //        int cif = rnd.Next(1, 6);
            //        aparitii[i]++;
            //        StandardGame[i][i].HidenPicture = 
            //    }
            return StandardGame;
        }
    }
}
