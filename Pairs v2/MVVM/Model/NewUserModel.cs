﻿using Pairs_v2.Commands;
using Pairs_v2.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Pairs_v2.MVVM.Model
{
    public class NewUserModel :BaseClassNotifyPropertyChanged
    {
        private ICommand closeWindowCommand;
        public ICommand CloseWindowCommand
        {
            get
            {
                if(closeWindowCommand == null)
                {
                    NewPlayerOperations newPlayerOperations = new NewPlayerOperations(this);
                    closeWindowCommand = new RelayCommand(newPlayerOperations.CloseWindow);
                }
                return closeWindowCommand;
            }
          
        }
    }   
}

