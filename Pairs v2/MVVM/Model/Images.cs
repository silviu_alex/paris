﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pairs_v2.MVVM.Model
{
    public class Images
    {
        private string Path { get; set; }
        private string ImageName { get; set; }
        public Images(string path, string imagename)
        {
            Path = path;
            ImageName = imagename;
        }
    }
}
