﻿using Pairs_v2.Commands;
using Pairs_v2.MVVM.View_Model;
using Pairs_v2.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Pairs_v2.MVVM.Model
{
    public class LogInModel : BaseClassNotifyPropertyChanged
    {
        private ICommand newUserCommand;
        public ICommand NewUserCommand
        {
            get
            {
                if (newUserCommand == null)
                {
                    LogInOperations operation = new LogInOperations(this);
                    newUserCommand = new RelayCommand(operation.OpenAddUserWindow);
                }
                return newUserCommand;
            }
        }

        private ICommand quitApplicationCommand;
        public ICommand QuitApplicationCommand
        {
            get
            {
                if (quitApplicationCommand == null)
                {
                    LogInOperations operations = new LogInOperations(this);
                    quitApplicationCommand = new RelayCommand(operations.QuitApplication);
                }
                return quitApplicationCommand;
            }
        }

        private ICommand nextAvatarCommand;
        public  ICommand NextAvatarCommand
        {
            get
            {
                LogInOperations operations = new LogInOperations(this);
                nextAvatarCommand = new RelayCommand(operations.RightArrowButton);
                return nextAvatarCommand;
            }   
        }

        private ICommand previousAvatarCommand;
        public  ICommand PreviousAvatarCommand
        {
            get
            {
                LogInOperations operations = new LogInOperations(this);
                previousAvatarCommand = new RelayCommand(operations.LeftArrowButton);
                return previousAvatarCommand;
            }
        }
    }
}
