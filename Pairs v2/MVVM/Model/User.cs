﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pairs_v2.MVVM.Model
{
    public class User
    {
        private string Username
        {
            get; set;
        }
        private AvatarM Avatar
        {
            get; set;
        }
        public  User(string username, AvatarM avatar)
        {
            Username = username;
            Avatar = avatar;
        }
    }
}
