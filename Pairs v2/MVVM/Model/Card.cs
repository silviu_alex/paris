﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pairs_v2.MVVM.Model
{
    public class Card : BaseClassNotifyPropertyChanged
    {
        private int coordI;
        private int coordJ;
        private string state;
        private string hidenPicture;
        private string currentPicture;
        public int CoordI
        {
            get
            {
                return coordI;
            }
            set
            {
                if (coordI != value)
                {
                    coordI = value;
                   OnPropertyChanged("CoordI");
                }
            }
        }
        public int CoordJ
        {
            get
            {
                return coordJ;
            }
            set
            {
                if (coordJ != value)
                {
                    coordJ = value;
                    OnPropertyChanged("CoordJ");
                }
            }
        }
        public string State
        {
            get
            {
                return state;
            }
            set
            {
                if (state != value)
                {
                    state = value;
                    OnPropertyChanged("State");
                }
            }
        }
        public string HidenPicture
        {
            get
            {
                return hidenPicture;
            }
            set
            {
                if (hidenPicture != value)
                {
                    hidenPicture = value;
                    OnPropertyChanged("HidenPicture");
                }
            }
        }
        public string CurrentPicture
        {
            get
            {
                return currentPicture;
            }
            set
            {
                if (currentPicture != value)
                {
                    currentPicture = value;
                    OnPropertyChanged("CurrentPicture");
                }
            }
        }
        public Card(int coordI, int coordJ, string state, string hidenPicture)
        {
            CoordI = coordI;
            CoordJ = coordJ;
            State = state;
            HidenPicture = hidenPicture;
            CurrentPicture = "C:\\Users\\Silviu-PC\\source\\Pairs v2\\ v2\\Images\\Back.jpg";
        }
    }
}
